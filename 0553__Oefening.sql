USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
UPDATE Baasjes
CROSS JOIN Huisdieren
SET Baasjes.Huisdieren_Id = Huisdieren.Id
WHERE Baasjes.Baasje = Huisdieren.Baasje;
SET SQL_SAFE_UPDATES = 1;