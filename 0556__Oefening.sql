USE ModernWays;
SELECT Huisdieren.Naam AS 'Naam Huisdieren', Baasjes.Baasje AS 'Naam Baasje'
FROM Huisdieren
INNER JOIN Baasjes
ON Huisdieren.Id = Baasjes.Huisdieren_Id;

