USE ModernWays;
SELECT AVG(Leeftijd) AS "Gemiddelde leeftijd", MAX(Leeftijd) AS "Oudste Huisdier", COUNT(*) AS "Totaal aantal"
FROM Huisdieren;