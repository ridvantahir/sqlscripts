USE ModernWays;
INSERT INTO Huisdieren (
Baasje,
Naam,
Leeftijd,
Soort
)
VALUE 
('Vincent','Misty', 6,'hond'),
('Schristina','Ming ', 8,'hond'),
('Esther','Bientje', 6,'kat'),
('Jommeke','Flip', 75,'papagaai'),
('Villads','Berto ', 1,'papagaai'),
('Bert','Ming', 7,'kat'),
('Thaïs','Suerta', 2,'hond'),
('Lyssa','Фёдор', 1,'hond')