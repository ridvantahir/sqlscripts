USE ModernWays;
CREATE Table Boeken(
Voornaam VARCHAR(50) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(80) CHAR SET utf8mb4 NOT NULL,
Titel VARCHAR(255) CHAR SET utf8mb4,
Categorie VARCHAR(50) CHAR SET utf8mb4
);
INSERT INTO Boeken(Voornaam,Familienaam,Titel,Categorie)
VALUES
('Stephen','Hawking','The Nature of Space and Time','Wiskunde'),
('Stephen','Hawking','Antwoorden op de grote vragen','Filosofie'),
('William','Dunham','Journey through Genius: The Great Theorems of Mathematics','Wiskunde'),
('William','Dunham','Euler: The Master of Us All','Geschiedenis'),
('Evert Willem','Beth','Mathematical Thought','Filosofie');