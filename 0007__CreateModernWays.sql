CREATE DATABASE ModernWays;
USE ModernWays;
Create table MijnBoeken(
Vooraam varchar(50) char set utf8mb4,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Title varchar(255) char set utf8mb4,
Stad varchar(50) char set utf8mb4,
    -- alleen het jaartal, geen datetime
    -- omdat de kleinste datum daarin 1753 is
    -- varchar omdat we ook jaartallen kleiner dan 1000 hebben
Verschrijningsjaar varchar(4),
Uitgeverij varchar(80) char set utf8mb4,
Herdruk varchar(4),
Commentaar varchar(1000)
);